package agenda.m3m6.fernandez.outumuro.inspedralbes.cat;

import java.util.ArrayList;
import java.util.List;

public class Grup {	
	private int codi;
	private String nom;
	private List<Contacte> contactes;
	
	public Grup(int codi, String nom) {
		this.codi = codi;
		this.nom = nom;
		contactes = new ArrayList<Contacte>();
	}
	
	public void agregarContacte(Contacte con) {
		int i=0;

		for (Contacte contacte : contactes) {
			if(contacte.getNif().equals(con.getNif())) {
				i=1;
			}
		}
				
		if(i==0) {
			contactes.add(con);
		}else {
			System.out.println("Ya esta almacenado este contacto en el grupo");
		}	
			
	}

	public int getCodi() {
		return codi;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	
}
