package agenda.m3m6.fernandez.outumuro.inspedralbes.cat;

public class Telefon {
	private int num;

	public Telefon(int num) {
		this.num = num;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

}
