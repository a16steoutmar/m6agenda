package agenda.m3m6.fernandez.outumuro.inspedralbes.cat;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrincipalTerminal {

	public static void main(String[] args) throws SQLException {
		int opcio;
		Scanner scan = new Scanner(System.in);
		BDManager bdManagerIncrustat = new HyperSQL();
		List<Telefon> telefons = new ArrayList<Telefon>();
		List<String> contactes = new ArrayList<String>();
		bdManagerIncrustat.creaBD();
		String nomGrup;

		do {

			System.out.println();
			System.out.println("+--------------------------------+");
			System.out.println("| 1. Esborra contacte            |");
			System.out.println("| 2. Mostra tots els contactes   |");
			System.out.println("| 3. Insereix contacte           |");
			System.out.println("| 4. Actualitza dades            |");
			System.out.println("| 5. Crea grup                   |");
			System.out.println("| 6. Esborra grup                |");
			System.out.println("| 7. Afegir contacte al grup     |");
			System.out.println("| 8. Treure contacte del grup    |");
			System.out.println("| 9. Mostra tot el grup          |");
			System.out.println("| 10. Sortir                     |");
			System.out.println("+--------------------------------+");

			System.out.print("Opcio? : ");
			opcio = scan.nextInt();
			// netejem scanner
			scan.nextLine();

			switch (opcio) {
			case 1:
				System.out.print("Indica el NIF: ");
				String nif = scan.nextLine();
				bdManagerIncrustat.borraContacte(nif);

				break;

			case 2:
				contactes = bdManagerIncrustat.mostraTotsContactes();
				Contacte contacte = null;
				for (String contact : contactes) {
					System.out.println(contact);

				}

				System.out.println("+---------------------------------------+");
				System.out.println("| Veure l'informacio del contacte per:  |");
				System.out.println("| 1. Nom                                |");
				System.out.println("| 2. Nif                                |");
				System.out.println("+---------------------------------------+");
				System.out.print("Opcio? : ");
				int opcio1 = Integer.parseInt(scan.nextLine());

				if (opcio1 == 1) {
					System.out.print("Indica el NOM del contacte que vols cercar: ");
					String nom = scan.nextLine();

					contacte = bdManagerIncrustat.mostraContacte(nom, opcio1);

				} else if (opcio1 == 2) {
					System.out.print("Indica el NIF del contacte que vols cercar: ");
					nif = scan.nextLine();

					contacte = bdManagerIncrustat.mostraContacte(nif, opcio1);

				} else {
					System.out.println("NUM INCORRECTE");
					break;
				}

				System.out.println("");

				if (contacte.getNif() == null) {
					System.out.println("El contacte no existeix");
				} else {
					System.out.println("NIF: " + contacte.getNif());
					System.out.println("NOM: " + contacte.getNom());
					System.out.println("CORREU: " + contacte.getCorreu());
					System.out.println("TELEFONS: ");
					for (Telefon num : contacte.getTelefons()) {
						System.out.println(num.getNum());
					}
					bdManagerIncrustat.mostraGrupDeContacte(contacte.getNif());
				}

				break;

			case 3:
				System.out.print("Indica el NIF: ");
				nif = scan.nextLine();

				System.out.print("Indica el nom: ");
				String nom = scan.nextLine();

				System.out.print("Indica correu: ");
				String correu = scan.nextLine();

				System.out.print("Quants telefons te: ");
				int quantitat = scan.nextInt();
				scan.nextLine();

				for (int i = 0; i < quantitat; i++) {
					System.out.print("Indica num: ");
					telefons.add(new Telefon(scan.nextInt()));
				}

				bdManagerIncrustat.nouContacte(new Contacte(nif, nom, correu, telefons));

				break;

			case 4:
				System.out.println("+----------------------+");
				System.out.println("| Que vols modificar:  |");
				System.out.println("| 1. Contacte          |");
				System.out.println("| 2. Grup              |");
				System.out.println("+----------------------+");
				System.out.print("Opcio? : ");
				opcio1 = scan.nextInt();
				// netejem scanner
				scan.nextLine();

				if (opcio1 == 1) {
					bdManagerIncrustat.modificaContacte();

				} else if (opcio1 == 2) {
					System.out.print("Indica el nom del grup que vols modificar: ");
					nomGrup = scan.nextLine();
					bdManagerIncrustat.modificaGrup(nomGrup);

				} else {
					System.out.println("NUM INCORRECTE");
				}
				break;

			case 5:
				System.out.print("Escriu el nom del grup a crear: ");
				nomGrup = scan.nextLine();

				bdManagerIncrustat.afegirGrup(nomGrup);
				break;

			case 6:
				System.out.print("Escriu el nom del grup a eliminar: ");
				nomGrup = scan.nextLine();

				bdManagerIncrustat.eliminarGrup(nomGrup);
				break;

			case 7:
				System.out.print("Indica el grup: ");
				nomGrup = scan.nextLine();

				System.out.print("Indica el NIF del contacte: ");
				nif = scan.nextLine();

				bdManagerIncrustat.afegirContacte(nomGrup, nif);
				break;

			case 8:

				System.out.print("Indica el NIF del contacte: ");
				nif = scan.nextLine();

				System.out.print("Indica el nom del grup: ");
				nomGrup = scan.nextLine();

				bdManagerIncrustat.treureContac(nif, nomGrup);

				break;

			case 9:
				System.out.print("Indica el grup: ");
				nomGrup = scan.nextLine();

				bdManagerIncrustat.consultaInfoGrup(nomGrup);
				break;

			case 10:
				System.out.println("Adeu!");
				break;

			default:
				System.out.println();
				System.out.println("Numero incorrecte, torna a probar");
				break;

			}

		} while (opcio != 10);
	}

}
