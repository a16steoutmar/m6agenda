package agenda.m3m6.fernandez.outumuro.inspedralbes.cat;

import java.sql.SQLException;
import java.util.List;

public interface BDManager {
	
	public void creaBD();
	
	public void nouContacte(Contacte contacteNou);
	
	public void borraContacte(String nif);
	
	public List<String> mostraTotsContactes();
	
	public Contacte mostraContacte(String nif, int opcio1) throws SQLException;

	public void afegirGrup(String grup) throws SQLException;

	public void eliminarGrup(String grup) throws SQLException;

	public void afegirContacte(String grup, String nif) throws SQLException;

	public void treureContac(String nif, String nomGrup) throws SQLException;

	public void consultaInfoGrup(String grup) throws SQLException;

	public void modificaContacte();

	public void modificaGrup(String nomGrup);

	public void mostraGrupDeContacte(String nif) throws SQLException;

	
}
