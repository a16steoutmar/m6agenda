package agenda.m3m6.fernandez.outumuro.inspedralbes.cat;

import java.util.List;

public class Contacte {
	private String nif;
	private String nom;
	private String correu;
	private List<Telefon> telefons;

	public Contacte(String nif, String nom, String correu, List<Telefon> telefons) {
		this.nif = nif;
		this.nom = nom;
		this.correu = correu;
		this.telefons = telefons;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCorreu() {
		return correu;
	}

	public void setCorreu(String correu) {
		this.correu = correu;
	}
	
	public List<Telefon> getTelefons() {
		return telefons;
	}

	public void setTelefons(List<Telefon> telefons) {
		this.telefons = telefons;
	}

}
