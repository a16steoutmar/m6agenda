package agenda.m3m6.fernandez.outumuro.inspedralbes.cat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HyperSQL implements BDManager {
	public static final String NOMBD = "agenda";
	public static final String NOMUSUARI = "SA";
	public static final String PASS = "";
//	public static final String URL = "jdbc:hsqldb:file:C:\\Users\\aestebansanchez\\Desktop\\fil\\" + NOMBD;
//	public static final String URL = "jdbc:hsqldb:file:C:\\Users\\Stefy\\Documents\\" + NOMBD;
	 private static final String URL = "jdbc:hsqldb:file:/home/ausias/Escriptori/"+ NOMBD;
	Connection con;
	Grup grup;

	public void conectar() {
		try {
			con = DriverManager.getConnection(URL, NOMUSUARI, PASS);
			// con = DriverManager.getConnection(URL, "SA", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void creaBD() {
		try {
			conectar();
			Statement statement = con.createStatement();

//			statement.executeUpdate("DROP TABLE IF EXISTS GRUP_CON;");
//			statement.executeUpdate("DROP TABLE IF EXISTS TELEFONS;");
//			statement.executeUpdate("DROP TABLE IF EXISTS CONTACTES;");
//			statement.executeUpdate("DROP TABLE IF EXISTS GRUPS;");

			// CREAR
			statement.executeUpdate(
					"CREATE TABLE IF NOT EXISTS CONTACTES (NIF VARCHAR(9), NOM VARCHAR(10), CORREU VARCHAR(20), PRIMARY KEY(NIF));");

//			statement.executeUpdate(
//					"CREATE TABLE IF NOT EXISTS TELEFONS (ID INTEGER, NUM INTEGER, NIF_CONTACTE VARCHAR(10), PRIMARY KEY(ID), FOREIGN KEY(NIF_CONTACTE) REFERENCES CONTACTES(NIF));");

			statement.executeUpdate(
					"CREATE TABLE IF NOT EXISTS TELEFONS (ID INTEGER, NUM INTEGER, NIF_CONTACTE VARCHAR(10), PRIMARY KEY(ID));");

			// CREATE TABLE GRUPS
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS GRUPS(CODI INTEGER PRIMARY KEY, NOM VARCHAR(30));");

			statement.executeUpdate(
					"CREATE TABLE IF NOT EXISTS GRUP_CON(NIF_CON VARCHAR(9), ID_GRUP INTEGER, PRIMARY KEY(NIF_CON, ID_GRUP), CONSTRAINT FK_GRU FOREIGN KEY(ID_GRUP) REFERENCES GRUPS(CODI));");

			// AFEGIR ALGUNS CONTACTES DE PROVA
//			statement.executeUpdate("INSERT INTO CONTACTES VALUES ('11111111D', 'Sil', 'sil@sil.com');");
//			statement.executeUpdate("INSERT INTO CONTACTES VALUES ('22222222F', 'Stef', 'stef@stef.com');");
//			statement.executeUpdate("INSERT INTO CONTACTES VALUES ('33333333G', 'Jeffry', 'jeffry@jeffry.com');");
//
//			statement.executeUpdate("INSERT INTO TELEFONS VALUES (1, 935801416, '11111111D');");
//			statement.executeUpdate("INSERT INTO TELEFONS VALUES (2, 935801417, '11111111D');");
//			statement.executeUpdate("INSERT INTO TELEFONS VALUES (3, 935801412, '22222222F');");
//			statement.executeUpdate("INSERT INTO TELEFONS VALUES (4, 935801498, '33333333G');");

			statement.close();
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void nouContacte(Contacte contacteNou) {
		try {
			conectar();
			Statement statement = con.createStatement();
			int id = 0;

			statement.executeUpdate("INSERT INTO CONTACTES VALUES ('" + contacteNou.getNif() + "', '"
					+ contacteNou.getNom() + "', '" + contacteNou.getCorreu() + "');");

			String autoin = "SELECT MAX(ID) AS CODI FROM TELEFONS;";
			ResultSet autoincrement = statement.executeQuery(autoin);

			if (autoincrement.next()) {
				id = autoincrement.getInt("CODI");		
			}

			for (Telefon telef : contacteNou.getTelefons()) {
				id = id + 1;
				statement.executeUpdate("INSERT INTO TELEFONS VALUES(" + id + ", " + telef.getNum() + ", '"
						+ contacteNou.getNif() + "');");

			}

			System.out.println("CONTACTE AFEGIT");
			statement.close();
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("CONTACTE NO afegit, REVISA LA INFORMACI� INTRODU�DA");
		}

	}

	@Override
	public void borraContacte(String nif) {
		try {
			conectar();
			Statement statement = con.createStatement();

			statement.executeUpdate("DELETE FROM GRUP_CON WHERE NIF_CON='" + nif + "';");
			statement.executeUpdate("DELETE FROM CONTACTES WHERE NIF = '" + nif + "';");
			statement.executeUpdate("DELETE FROM TELEFONS WHERE NIF_CONTACTE = '" + nif + "';");

			System.out.println("Contacte el�liminat");
			con.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("NO EXISTEIX AQUEST CONTACTE O T'HAS EQUIVOCAT");
		}

	}

	@Override
	public List<String> mostraTotsContactes() {
		List<String> contactes = new ArrayList<String>();

		try {
			conectar();
			Statement statement = con.createStatement();

			ResultSet rs = statement.executeQuery("SELECT * FROM CONTACTES;");

			while (rs.next()) {
				String nif = rs.getString("NIF");
				String nom = rs.getString("NOM");
				contactes.add(nif + ", " + nom);
			}

			rs.close();
			statement.close();
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return contactes;
	}

	public void mostraGrupDeContacte(String nif) throws SQLException {
		conectar();
		Statement statement = con.createStatement();
		boolean pertany = false;

		ResultSet rs = statement.executeQuery(
				"SELECT G.NOM FROM GRUPS G, CONTACTES C, GRUP_CON A WHERE G.CODI=A.ID_GRUP AND C.NIF=A.NIF_CON AND C.NIF ='"
						+ nif + "';");

		while (rs.next()) {
			if (!pertany) {
				System.out.println("Pertany al grup:");
			}
			String nom = rs.getString("NOM");
			System.out.println(nom);
			pertany = true;
		}

		if (!pertany) {
			System.out.println("No pertany a cap grup");
		}

	}

	@Override
	public Contacte mostraContacte(String valor, int opcio) throws SQLException {
		List<Telefon> telefons = new ArrayList<Telefon>();
		String nom = null;
		String correu = null;
		String nif = null;
		Contacte cont = null;

		ResultSet rs = null;
		conectar();
		Statement statement = con.createStatement();

		if (opcio == 1) {
			rs = statement.executeQuery("SELECT * FROM CONTACTES WHERE NOM = '" + valor + "';");
		}else {
			rs = statement.executeQuery("SELECT * FROM CONTACTES WHERE NIF = '" + valor + "';");
		}
		
		while (rs.next()) {
			nif = rs.getString("NIF");
			nom = rs.getString("NOM");
			correu = rs.getString("CORREU");
		}

		rs = statement.executeQuery("SELECT NUM FROM TELEFONS WHERE NIF_CONTACTE = '" + nif + "';");

		while (rs.next()) {
			telefons.add(new Telefon(rs.getInt("NUM")));
		}

		rs.close();
		statement.close();
		con.close();

		return new Contacte(nif, nom, correu, telefons);
	}

	//////////////////////////////////// GRUPS/////////////////////////////////////////////////////////

	// Añado un usuario a la clase grupo, y mediante una tercera tabla relaciono el
	// usuario al grupo
	public void afegirContacte(String grupNom, String nif) throws SQLException {
		conectar();
		Statement stmt = con.createStatement();

		boolean troba = false;
		int codi = 0;
		String nom = null;
		String correu = null;
		List<Telefon> buit = new ArrayList<Telefon>();

		String obtenNom = "SELECT CODI FROM GRUPS WHERE NOM='" + grupNom + "';";
		ResultSet no = stmt.executeQuery(obtenNom);

		String verificaNif = "SELECT NOM, CORREU FROM CONTACTES WHERE NIF='" + nif + "';";
		ResultSet existeix = stmt.executeQuery(verificaNif);

		if (no.next()) {
			codi = no.getInt("CODI");
			troba = true;
		}

		while (existeix.next()) {
			buit.add(new Telefon(0));
			nom = existeix.getString("NOM");
			correu = existeix.getString("CORREU");
			troba = true;
		}

		// Creo la relacion entre contacto y grupo
		if (troba) {

			String cons = "SELECT NIF_CON, ID_GRUP FROM GRUP_CON WHERE NIF_CON = '" + nif + "' AND ID_GRUP =" + codi
					+ ";";
			ResultSet rs = stmt.executeQuery(cons);

			if (rs.next() == false) {
				// Aqui lo agrego a la clase grupo
				grup = new Grup(codi, grupNom);
				grup.agregarContacte(new Contacte(nif, nom, correu, buit));
				String datos = "INSERT INTO GRUP_CON(NIF_CON, ID_GRUP) VALUES ('" + nif + "', " + codi + ");";
				stmt.executeUpdate(datos);
				System.out.println("Uni� creada");

			} else {
				System.out.println("CONTACTE JA AFEGIT");
			}
		}

		stmt.close();
		con.close();
	}

	// Creo un grupo y lo añado a la BD
	public void afegirGrup(String nom) throws SQLException {
		conectar();
		Statement stmt = con.createStatement();
		String nomBD = null;
		int id = 0;

		String cons = "SELECT NOM FROM GRUPS WHERE NOM = '" + nom + "';";
		ResultSet rs = stmt.executeQuery(cons);

		while (rs.next()) {
			nomBD = rs.getString("NOM");
		}

		//////////////// CREA GRUP SI EXISTEIX O NO ////////////////

		if (nomBD == null) {
			String autoin = "SELECT MAX(CODI) AS ID FROM GRUPS;";
			ResultSet autoincrement = stmt.executeQuery(autoin);

			if (autoincrement.next()) {
				id = autoincrement.getInt("ID");
				id = id + 1;
			}

			grup = new Grup(id, nom);

			String datos = "INSERT INTO GRUPS (CODI, NOM) VALUES (" + grup.getCodi() + ", '" + grup.getNom() + "');";
			stmt.executeUpdate(datos);
			System.out.println("Grup creat");

		} else {
			System.out.println("Ya esta emmagatzemat aquest grup");
		}

		stmt.close();
		con.close();
	}

	public void eliminarGrup(String nom) throws SQLException {
		conectar();
		Statement stmt = con.createStatement();

		int codi = 0;
		boolean troba = false;

		String obtenNom = "SELECT CODI FROM GRUPS WHERE NOM='" + nom + "';";
		ResultSet rs = stmt.executeQuery(obtenNom);

		if (rs.next()) {
			codi = rs.getInt("CODI");
			troba = true;
		}

		if (troba) {
			String con_gru = "DELETE FROM GRUP_CON WHERE ID_GRUP=" + codi + ";";
			stmt.executeUpdate(con_gru);

			String grup = "DELETE FROM GRUPS WHERE CODI=" + codi + ";";
			stmt.executeUpdate(grup);
			System.out.println("Grup " + nom + " el�liminat");

		} else {
			System.out.println("El grup indicat no existeix");
		}

		stmt.close();
		con.close();
	}

	public void treureContac(String nif, String nomGrup) throws SQLException {
		conectar();
		Statement stmt = con.createStatement();
		boolean trobat = false;
		int codi = 0;

		ResultSet rs = stmt.executeQuery("SELECT NIF FROM CONTACTES WHERE NIF= '" + nif + "';");

		if (rs.next()) {
			trobat = true;
		}

		String obtenNom = "SELECT CODI FROM GRUPS WHERE NOM='" + nomGrup + "';";
		rs = stmt.executeQuery(obtenNom);

		trobat = false;
		if (rs.next()) {
			codi = rs.getInt("CODI");
			trobat = true;
		}

		if (trobat) {
			String cont = "DELETE FROM GRUP_CON WHERE NIF_CON= '" + nif + "' AND ID_GRUP= " + codi + ";";
			stmt.executeUpdate(cont);
			System.out.println("Contacte el�liminat del grup " + nomGrup);
		} else {
			System.out.println("No existex aquest nif en la base de dades");
		}

		stmt.close();
		con.close();
	}

	public void consultaInfoGrup(String grup) throws SQLException {
		conectar();
		Statement stmt = con.createStatement();
		boolean hiHaContacte = false;

		ResultSet rs = stmt.executeQuery("SELECT CODI FROM GRUPS WHERE NOM LIKE '" + grup + "';");

		boolean trobat = false;
		int codi = 0;

		if (rs.next()) {
			codi = rs.getInt("CODI");
			trobat = true;
		}

		if (trobat) {
			String cons = "SELECT NIF_CON FROM GRUP_CON WHERE ID_GRUP=" + codi + ";";
			ResultSet rs1 = stmt.executeQuery(cons);

			System.out.println("El grup: " + grup + " esta format per: ");
			while (rs1.next()) {
				System.out.println(rs1.getNString("NIF_CON"));
				hiHaContacte = true;
			}
			if (!hiHaContacte) {
				System.out.println("No hi ha contactes en aquest grup");
			}
		} else {
			System.out.println("NO EXISTEIX EL GRUP AMB AQUEST NOM");
		}
	}

	@Override
	public void modificaContacte() {
		conectar();
		Scanner scan = new Scanner(System.in);

		int opcio = 0;
		boolean repetit = false;
		boolean existeix = false;
		int j = 0;

		try {
			Statement statement = con.createStatement();

			System.out.print("Indica el NIF de la persona que vols modificar: ");
			String nif = scan.nextLine();

			ResultSet rs = statement.executeQuery("SELECT NIF FROM CONTACTES WHERE NIF = '" + nif + "';");

			if (rs.next()) {
				nif = rs.getString("NIF");
				repetit = true;
			}

			if (repetit) {
				do {
					System.out.println("+----------------------+");
					System.out.println("| Que vols modificar:  |");
					System.out.println("| 1. Nom               |");
					System.out.println("| 2. Correu            |");
					System.out.println("| 3. Telefons          |");
					System.out.println("| 4. Ja esta           |");
					System.out.println("+----------------------+");
					System.out.print("Opcio? : ");
					opcio = scan.nextInt();
					// netejem scanner
					scan.nextLine();

					switch (opcio) {
					case 1:
						System.out.print("Indica el nou Nom: ");

						rs = statement.executeQuery(
								"UPDATE CONTACTES SET NOM = '" + scan.nextLine() + "' WHERE NIF= '" + nif + "';");
						System.out.println("NOM ACTUALITZAT");

						break;
					case 2:
						System.out.print("Indica el nou correu: ");

						rs = statement.executeQuery(
								"UPDATE CONTACTES SET CORREU = '" + scan.nextLine() + "' WHERE NIF= '" + nif + "';");
						System.out.println("CORREU ACTUALITZAT");

						break;
					case 3:
						int codi = 0;
						System.out.print("Indica el num de telefon que vols modificar: ");

						rs = statement.executeQuery("SELECT ID FROM TELEFONS WHERE NUM = " + scan.nextLine()
								+ " AND NIF_CONTACTE= '" + nif + "';");

						if (rs.next()) {
							codi = rs.getInt("ID");
							repetit = true;
						}

						if (repetit) {
							System.out.print("Indica el nou telefon que vols posar: ");
							rs = statement.executeQuery(
									"UPDATE TELEFONS SET NUM = " + scan.nextLine() + " WHERE ID= " + codi + ";");
							System.out.println("TELEFON ACTUALITZAT");
						}

						break;

					case 4:
						System.out.println("Anant enrere...");
						break;

					default:
						System.out.println();
						System.out.println("Numero incorrecte, torna a probar");
						break;
					}

				} while (opcio != 4 || opcio < 4);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("NO MODIFICAT, REVISA LA INFORMACI� INTRODU�DA");
		}

	}

	@Override
	public void modificaGrup(String nomGrup) {
		conectar();
		Scanner scan = new Scanner(System.in);
		boolean existeix = false;
		int codi = 0;

		try {
			Statement statement = con.createStatement();

			ResultSet rs = statement.executeQuery("SELECT CODI FROM GRUPS WHERE NOM = '" + nomGrup + "';");

			if (rs.next()) {
				codi = rs.getInt("CODI");
				existeix = true;
			}

			if (existeix) {
				System.out.print("Indica el nou nom del grup que vols posar: ");
				rs = statement
						.executeQuery("UPDATE GRUPS SET NOM = '" + scan.nextLine() + "' WHERE CODI= " + codi + ";");
				System.out.println("NOM GRUP ACTUALITZAT");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("NO ACTUALITZAT, HI HA ALGUN PROBLEMA, REVISA LA INFORMACI� INTRODU�DA");
		}

	}

}
